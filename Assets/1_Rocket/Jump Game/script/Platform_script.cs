﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_script : MonoBehaviour
{
    public Setting_pos set_script;

    public bool moving = false;
    public Vector3 target_pos;

    private bool shouldLerp = false;

    public float timeStartedLerping;
    public float lerpTime;

    public Vector3 endPosition;
    public Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(moving)
        {
            transform.position = Lerp(startPosition, endPosition, timeStartedLerping, lerpTime);
            //transform.position -= new Vector3(0, 1.0f * Time.deltaTime, 0);
            if (transform.position.y <= endPosition.y)
            {
                moving = false;
            }
        }
        //transform.position += new Vector3(0, 0.5f*Time.deltaTime, 0);
    }

    public void moveDown()
    {
        //transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, transform.position.y-2, transform.position.z), 0.5f);
        //target_pos = new Vector3(transform.position.x, transform.position.y-2, transform.position.z);
        startPosition = transform.position;
        endPosition = new Vector3(transform.position.x, transform.position.y-2.0f, set_script.z_pos);
        //moving = true;
        StartLerping();
    }


    private void StartLerping()
    {
        timeStartedLerping = Time.time;

        shouldLerp = true;
        moving = true;
    }

    public Vector3 Lerp(Vector3 start, Vector3 end, float timeStartedLerping, float lerpTime = 1)
    {
        float timeSinceStarted = Time.time - timeStartedLerping;

        float percentageComplete = timeSinceStarted / lerpTime;

        var result = Vector3.Lerp(start, end, percentageComplete);

        return result;
    }
}
