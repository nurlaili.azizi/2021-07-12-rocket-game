﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHead : MonoBehaviour
{
    public Transform Head_pos;

    // Start is called before the first frame update
    void Start()
    {
        Head_pos = GameObject.FindGameObjectWithTag("webarface").transform;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(Head_pos.position.x*35, (Head_pos.position.y*35)+35, /*transform.position.z*/Head_pos.position.z*35);
        //transform.position = Head_pos.position;
        transform.position = new Vector3(Head_pos.position.x, Head_pos.position.y + 1.0f, Head_pos.position.z);
        transform.rotation = Head_pos.rotation;
    }
}
