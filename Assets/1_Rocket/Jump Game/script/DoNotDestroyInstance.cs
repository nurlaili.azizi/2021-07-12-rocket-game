﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotDestroyInstance : MonoBehaviour {

    public string mytag = "";

	private static DoNotDestroyInstance instance = null;
	public static DoNotDestroyInstance Instance {
		get { return instance; }
	}
	void Awake() {
        /*		if (instance != null && instance != this) {
                    Destroy(this.gameObject);
                    return;
                } else {
                    instance = this;
                }*/
        GameObject[] myobject = GameObject.FindGameObjectsWithTag(mytag);
        if(myobject.Length > 1)
        {
            Destroy(this.gameObject);
        }
		DontDestroyOnLoad(this.gameObject);
	}
}
