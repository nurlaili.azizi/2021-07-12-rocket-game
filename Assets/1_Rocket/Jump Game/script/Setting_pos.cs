﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting_pos : MonoBehaviour
{
    public float z_pos = 0f;
    public bool first_setting = false;

    public bool CanPlay = false;

    public Transform Head_detect;
    // Start is called before the first frame update
    void Start()
    {
        //CanPlay = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void changing_pos_z()
    {
        if(!first_setting)
        {
            z_pos = Head_detect.position.z;
            first_setting = true;
        }
    }
}
