﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class set_player_pos : MonoBehaviour
{
    public Platform_script platform_move_script;
    public spawn_platform[] spawnplatform_script;
    public Setting_pos set_z_script;
    public Transform Head_pos;

    public UI_Controller UIScript;

    public float jump_force = 0f;
    Rigidbody2D rb;
    public bool isjumping = true;

    public float movespeed;

    public AudioSource player_jump_Sound;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(set_z_script.CanPlay)
        {
            Vector3 targetpos = new Vector3(Head_pos.position.x, transform.position.y, set_z_script.z_pos);
            //transform.position = targetpos;
            transform.position = Vector3.Lerp(transform.position, targetpos, movespeed*Time.deltaTime);
        }
        
    }

    public void Jump_player()
    {
        if(!isjumping)
        {
            rb.AddForce(new Vector2(0, jump_force));
            isjumping = true;
        }
    }

    public void SetInitialPlayer_pos()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, set_z_script.z_pos);
    }
    


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "ground")
        {
            if(set_z_script.CanPlay)
            {
                if (rb.velocity.y == 0 && isjumping)
                {
                    UIScript.AddScore();
                    start_jumping_Feature();
                }
            }
        }
    }

    public void start_jumping_Feature()
    {
        isjumping = false;
        StartCoroutine(delay_jump());
    }

    IEnumerator delay_jump()
    {
        yield return new WaitForSeconds(0.5f);
        if(set_z_script.CanPlay)
        {
            player_jump_Sound.Play();
            Jump_player();

            int a = Random.Range(0, spawnplatform_script.Length);
            spawnplatform_script[a].SpawnPlatform_Function();
            platform_move_script.moveDown();
        } 
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "loseboundry")
        {
            set_z_script.CanPlay = false;
            UIScript.EndGame();
        }
    }
}
