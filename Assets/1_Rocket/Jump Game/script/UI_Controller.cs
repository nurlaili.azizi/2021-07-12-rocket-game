﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using TMPro;
using System.Runtime.InteropServices;

public class UI_Controller : MonoBehaviour
{
    public Setting_pos set_script;

    public GameObject homePage;
    public GameObject Gamepage;
    public GameObject ResultPage;
    public GameObject CountdownPage;
    public TMP_Text Result_Score;

    public set_player_pos player_script;

    public float init_time = 0f;
    private float timer = 0f;
    private float filled_amount = 0f;
    public GameObject Timer_Indicator;

    public int score = 0;

    public AudioSource ButtonClick;
    public AudioSource WinSound;
    public AudioSource LoseSound;

    public string game_result = "";
    public string voucher_ID = "";

    [DllImport("__Internal")]
    private static extern void gameEndSubmitScore(int gameScore);

    private void Awake()
    {
        /*string atemp_url = Application.absoluteURL;
        string[] btemp = atemp_url.Split('?');
        string[] ctemp = btemp[1].Split('&');
        foreach (string dtemp in ctemp)
        {
            string[] etemp = dtemp.Split('=');
            if (etemp[0] == "voucherID")
            {
                voucher_ID = etemp[1];
            }
        }*/
    }

    // Start is called before the first frame update
    private void Start()
    {
        //Gamepage = GameObject.FindGameObjectWithTag("gamepage");
        //Gamepage.SetActive(false);
        timer = init_time;
    }

    // Update is called once per frame
    private void Update()
    {
        if (set_script.CanPlay)
        {
            timer -= Time.deltaTime;
            filled_amount = timer / init_time;
            Timer_Indicator.GetComponent<Image>().fillAmount = filled_amount;
            if (timer <= 0)
            {
                timer = 0;
                set_script.CanPlay = false;
                EndGame();
            }
        }
    }

    public void StartButton()
    {
        homePage.SetActive(false);
        Gamepage.SetActive(true);
        ButtonClick.Play();
        StartCoroutine(Countdown());
    }

    private IEnumerator Countdown()
    {
        CountdownPage.SetActive(true);
        yield return new WaitForSeconds(3.0f);
        CountdownPage.SetActive(false);
        set_script.CanPlay = true;
        player_script.start_jumping_Feature();
    }

    public void EndGame()
    {
        if (score >= 5)
        {
            game_result = "win";
        }
        else
        {
            game_result = "lose";
        }
        WinSound.Play();
        Gamepage.SetActive(false);
        ResultPage.SetActive(true);
        Result_Score.text = score.ToString();

#if UNITY_WEBGL && !UNITY_EDITOR
        gameEndSubmitScore(score);
#endif
    }

    public void AddScore()
    {
        score += 10;
    }

    public void RedirectURL()
    {
        Application.OpenURL("https://trx.uid-testing.space/lobby");
        //Application.OpenURL("http://webargame.uid-testing.space/redeem?game_result=" + game_result + "&game_score=" + score.ToString() + "&voucherID=" + voucher_ID + "&game_id=3");
    }

    public void restartscene()
    {
        SceneManager.LoadScene("testing_doodle");
    }
}