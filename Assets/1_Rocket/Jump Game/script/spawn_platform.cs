﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn_platform : MonoBehaviour
{
    public GameObject platform_parent;
    public Setting_pos script_pos;
    public float spawn_delay = 0f;
    public GameObject spawn_platform_object;

    public bool first_time_wait = false;
    public float first_time_spawn_delay = 0f;

    // Start is called before the first frame update
    void Start()
    {
        //StartCoroutine(Spawning_System());
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, script_pos.z_pos);
    }
    IEnumerator Spawning_System()
    {
        if(!first_time_wait)
        {
            yield return new WaitForSeconds(first_time_spawn_delay);
            first_time_wait = true;
        }
        while(true)
        {
            GameObject temp = Instantiate(spawn_platform_object, transform.position, Quaternion.identity);
            temp.transform.parent = platform_parent.transform;
            yield return new WaitForSeconds(spawn_delay);
        }
    }
    public void SpawnPlatform_Function()
    {
        GameObject temp = Instantiate(spawn_platform_object, transform.position, Quaternion.identity);
        temp.transform.parent = platform_parent.transform;
    }
}
