﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class first_platform : MonoBehaviour
{
    bool z_change = false;
    public Transform headpos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void z_pos_change()
    {
        if(!z_change)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, headpos.position.z);
            z_change = true;
        }
    }
}
